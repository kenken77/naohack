/**
 * Created by phangty on 3/11/16.
 */
var config = require('./config'),
    fs = require('fs'),
    util = require('util'),
    AWS = require('aws-sdk'),
    exec = require('child_process').exec;


function home(response) {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });
    response.end(fs.readFileSync('./public/index.html'));
}

// this function uploads files

function upload(response, postData) {
    var files = JSON.parse(postData);
    console.log(" ---- upload --- ");
    // writing audio file to disk
    _upload(response, files.audio);
    console.log(" ---- upload 2--- ");
    if (files.uploadOnlyAudio) {
        response.statusCode = 200;
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(files.audio.name);
    }
    console.log(" ---- upload 3--- ");
    if (!files.uploadOnlyAudio) {
        // writing video file to disk
        _upload(response, files.video);

        var mergeFile =  merge(response, files);
        console.log(" ---- upload 4--- ");
        //console.log(mergeFile);
        //sendToS3Bucket(mergeFile);
    }
}

function sendToS3Bucket(file){
    //console.log(files.video);
    /*
    var streamingVideo = fs.createReadStream('video.webm');
    var s3Obj = new AWS.S3({params: {Bucket: 'nus-stackup', Key: 'video.webm'}});
    s3Obj.upload({Body: streamingVideo}).on('httpUploadProgress', function(evt){
        console.log(evt);
    }).send(function(err, data){console.log(data);});*/

}

// this function merges wav/webm files

function merge(response, files) {
    // detect the current operating system
    var isWin = !!process.platform.match( /^win/ );

    if (isWin) {
        ifWin(response, files);
    } else {
        ifMac(response, files);
    }
}

function _upload(response, file) {
    var fileRootName = file.name.split('.').shift(),
        fileExtension = file.name.split('.').pop(),
        filePathBase = config.upload_dir + '/',
        fileRootNameWithBase = filePathBase + fileRootName,
        filePath = fileRootNameWithBase + '.' + fileExtension,
        fileID = 2,
        fileBuffer;

    while (fs.existsSync(filePath)) {
        filePath = fileRootNameWithBase + '(' + fileID + ').' + fileExtension;
        fileID += 1;
    }

    file.contents = file.contents.split(',').pop();

    fileBuffer = new Buffer(file.contents, "base64");

    fs.writeFileSync(filePath, fileBuffer);
}

function serveStatic(response, pathname) {

    var extension = pathname.split('.').pop(),
        extensionTypes = {
            'js': 'application/javascript',
            'webm': 'video/webm',
            'mp4': 'video/mp4',
            'wav': 'audio/wav',
            'ogg': 'audio/ogg',
            'gif': 'image/gif'
        };

    response.writeHead(200, {
        'Content-Type': extensionTypes[extension]
    });
    if (hasMediaType(extensionTypes[extension]))
        response.end(fs.readFileSync('.' + pathname));
    else
        response.end(fs.readFileSync('./public' + pathname));
}

function hasMediaType(type) {
    var isHasMediaType = false;
    ['audio/wav', 'audio/ogg', 'video/webm', 'video/mp4'].forEach(function(t) {
        if(t== type) isHasMediaType = true;
    });

    return isHasMediaType;
}

function ifWin(response, files) {
    // following command tries to merge wav/webm files using ffmpeg
    var merger = __dirname + '\\merger.bat';
    var audioFile = __dirname + '\\uploads\\' + files.audio.name;
    var videoFile = __dirname + '\\uploads\\' + files.video.name;
    var mergedFile = __dirname + '\\uploads\\' + files.audio.name.split('.')[0] + '-merged.webm';

    // if a "directory" has space in its name; below command will fail
    // e.g. "c:\\dir name\\uploads" will fail.
    // it must be like this: "c:\\dir-name\\uploads"
    var command = merger + ', ' + audioFile + " " + videoFile + " " + mergedFile + '';
    exec(command, function (error, stdout, stderr) {
        if (error) {
            console.log(error.stack);
            console.log('Error code: ' + error.code);
            console.log('Signal received: ' + error.signal);
        } else {
            response.statusCode = 200;
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(files.audio.name.split('.')[0] + '-merged.webm');

            fs.unlink(audioFile);
            fs.unlink(videoFile);
            //return mergedFile;
        }
    });
}

function ifMac(response, files) {
    console.log("iMac ---> ");
    // its probably *nix, assume ffmpeg is available
    var audioFile = __dirname + '/uploads/' + files.audio.name;
    var videoFile = __dirname + '/uploads/' + files.video.name;
    //var mergedFile = __dirname + '/uploads/' + files.audio.name.split('.')[0] + '-merged.webm';
    var mergedFile = __dirname + '/uploads/final.webm';

    var util = require('util'),
        exec = require('child_process').exec;

    //-i left.mp4 -vf "[in] pad=2*iw:ih [left]; movie=right.mp4 [right];[left][right] overlay=main_w/2:0 [out]" output.mp4

    var command = "ffmpeg -i " + audioFile + " -i " + videoFile + " -map 1:a -map 0:v -c:a copy " + mergedFile;
    //var command = "ffmpeg -i " + audioFile + " -i " + videoFile + " -c:v libx264 -profile:v high -crf 18 -pix_fmt yuv420p  " + mergedFile;

    exec(command, function (error, stdout, stderr) {
        if (stdout) console.log(stdout);
        if (stderr) console.log(stderr);

        if (error) {
            console.log('exec error: ' + error);
            response.statusCode = 404;
            response.end();

        } else {
            response.statusCode = 200;
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(files.audio.name.split('.')[0] + '-merged.webm');

            // removing audio/video files
            fs.unlink(audioFile);
            fs.unlink(videoFile);
            //return mergedFile;
        }

    });
}

exports.home = home;
exports.upload = upload;
exports.serveStatic = serveStatic;
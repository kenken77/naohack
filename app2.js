/**
 * Created by phangty on 4/11/16.
 */
function Employee(x){
    this.department = x;
}

Employee.prototype = {
    getFullName : function(){
        var firstName = "Kenneth",
            lastName = "Phang"

        return firstName + " " + lastName;
    },

    isRich : function() {
        if (this.salary > 2000){
            return true;
        } else {
            return false;
        }
    }
};

function Person(a, b){
    Employee.call(this, a);
    this.department = b;
}
Person.prototype = Object.create(Employee.prototype, {
    salary : {
        value: 2000,
        enumerable: true,
        configurable: true,
        writable: true
    },
    getFullName : {
        value: function(){ // override
            return Employee.prototype.getFullName.apply(this, arguments); // call super
        },
        enumerable: true,
        configurable: true,
        writable: true
    }
});
Person.prototype.constructor = Person;

var person = new Person();
console.log(person.getFullName());